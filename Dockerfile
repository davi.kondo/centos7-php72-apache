FROM centos:7

ENV LC_ALL=en_US.utf8

ARG timezone
ENV timezone ${timezone:-"America/Sao_Paulo"}

RUN yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
    http://rpms.remirepo.net/enterprise/remi-release-7.rpm \
    yum-utils \
    && yum-config-manager --enable remi-php72 \
    && yum install -y php php-mcrypt php-cli php-gd php-curl php-ldap php-zip php-fileinfo php-pgsql php-opcache php-pear php-mbstring httpd tzdata php-mssql php-memcached \
    && echo ${timezone} > /etc/timezone \
    && sed -i "s|;date.timezone =|date.timezone = ${timezone}|g" /etc/php.ini \
    && sed -i "s|ErrorLog \"logs/error_log\"|ErrorLog \"/dev/stderr\"|g" /etc/httpd/conf/httpd.conf \ 
    && curl http://acraiz.icpbrasil.gov.br/credenciadas/RAIZ/ICP-Brasil.crt -o /etc/pki/ca-trust/source/anchors/ICP-Brasil.crt \
    && curl http://acraiz.icpbrasil.gov.br/credenciadas/RAIZ/ICP-Brasilv2.crt -o /etc/pki/ca-trust/source/anchors/ICP-Brasilv2.crt \
    && curl http://acraiz.icpbrasil.gov.br/credenciadas/RAIZ/ICP-Brasilv4.crt -o /etc/pki/ca-trust/source/anchors/ICP-Brasilv4.crt \
    && curl http://acraiz.icpbrasil.gov.br/credenciadas/RAIZ/ICP-Brasilv5.crt -o /etc/pki/ca-trust/source/anchors/ICP-Brasilv5.crt \
    && curl http://acraiz.icpbrasil.gov.br/credenciadas/RAIZ/ICP-Brasilv6.crt -o /etc/pki/ca-trust/source/anchors/ICP-Brasilv6.crt \
    && curl http://acraiz.icpbrasil.gov.br/credenciadas/RAIZ/ICP-Brasilv7.crt -o /etc/pki/ca-trust/source/anchors/ICP-Brasilv7.crt \
    && update-ca-trust \
    && yum clean all \
    && rm -rf /var/cache/yum

WORKDIR /var/www/html

EXPOSE 80/tcp

CMD [ "/usr/sbin/apachectl","-DFOREGROUND" ]